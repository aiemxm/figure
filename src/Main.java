import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int eAsc = 0;
        int nbLignesPair = 20;
        int nbLignesImpair = 19;
        int moitieLignes = (nbLignesPair / 2) - 1;
        int eDesc = moitieLignes;
        String etoile = "*";
        String pair = new String("pair");
        String impair = new String("impair");
        String parite = saisirChaine();

        System.out.println(parite);
        if (parite.equals(pair)) {
            System.out.println("coucou");
            System.out.println("ALLEZ C'EST PARTI");
            for (int i = 0; i < nbLignesPair; i++) {
                if (eAsc < (nbLignesPair / 2) - 1) {
                    if (i == 0) {
                        System.out.println("*");
                    }
                    etoile = etoile + "*";
                    System.out.print(etoile);
                    System.out.println();
                    eAsc++;


                } else if (eAsc >= moitieLignes) {
                    if (eAsc == eDesc) {
                        System.out.println(etoile);
                    }
                    System.out.println(etoile.substring(0, eDesc < 0 ? 0 : eDesc));
                    eDesc--;
                }

            }
        }  else if(parite.equals(impair)){
            System.out.println("Oh, voici la figure qui correspond a " + nbLignesImpair + ", c'est un nombre impair !");
            for (int i = 0; i < nbLignesImpair; i++) {
                if (eAsc < (nbLignesImpair / 2)) {
                    if (i == 0) {
                        System.out.println("*");
                    }
                    etoile = etoile + "*";
                    System.out.print(etoile);
                    System.out.println();
                    eAsc++;


                } else if (eAsc >= moitieLignes) {
                    System.out.println(etoile.substring(0, eDesc < 0 ? 0 : eDesc));
                    eDesc--;
                }

            }
        }

    }


    public static String saisirChaine() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Saisir une chaine : ");
        String s = null;
        try {
            s = scanner.nextLine();
        } catch (Exception e) {
            System.out.println("Erreur : " + e.getMessage());
        }
        scanner.close();
        return s;
    }
}

